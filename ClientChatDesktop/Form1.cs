﻿using Microsoft.AspNet.SignalR.Client;
using Microsoft.VisualBasic;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ClientChatDesktop
{
    public partial class Form1 : Form
    {
        private HubConnection Connection;
        private IHubProxy myHubProxy;
        private List<string> listMessage = new List<string>();
        public Form1()
        {
            InitializeComponent();
            label1.Text = Interaction.InputBox("Ingrese su nombre", "Chat", "Chat desktop");

            this.Connection = new HubConnection("http://localhost:54185/");
            this.myHubProxy = this.Connection.CreateHubProxy("ChatHub");

            this.myHubProxy.On<string, string>("broadcastMessage", (name, message) =>
                LlenarMensaje(name, message)
            );

            this.Connection.Start().Wait();
        }

        private void Actualizar()
        {
            listBox1.Items.Clear();
            foreach (var item in listMessage)
            {
                listBox1.Items.Add(item);
            }
        }

        private void LlenarMensaje(string name, string message)
        {
            listMessage.Add(name + ": " + message);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (textBox1.Text.Trim() != string.Empty)
                this.myHubProxy.Invoke("Send", label1.Text, textBox1.Text).Wait();
            textBox1.Clear();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Actualizar();
        }
    }
}
